# Wiki-Philo Web Crawler App #

Simple Flask and Jquery web application that crawls Wikipedia to find the 'degrees' of separation between a starting page and the philosophy Wikipedia page.

[Demo](https://wiki-philo.herokuapp.com/)

## How Does It Work? ##

Given an initial Wikipedia page, the web app attempts to locate the first Wikipedia link in the body of the page. 
Because of Wikipedia's standard format, the first link on a page tends to be a more generalized classification/category than the parent page. 
Using this logic, one can traverse from almost any Wikipedia page (specific topic) all the way to philosophy (general topic) by following the first link in each respective page. Most pages tend to reach philosophy within 9 - 20 'hops'.

## But Why? ##

When I was first learning how to program, I came across this basic Wikipedia web crawler exercise and made my own python command line script. Years later, I was looking for something to build and decided I should rewrite this project into a web application with full gui.

## Recent Updates ##

* Added redis cache to ease demand on Wikipedia
* Better url encoding on both client and app side
* Better exception handling including rendering on client-side
* Fixed mobile scroll override handle delay

## TODO ##

* Cover/prevent more breaking edge cases including foreign languages
* Capture usage data to compile statistics
* Look into using localized wiki database instead of api (wiki api slows dramatically when having issues)
* ~~Port to python 3 to auto avoid unicode errors~~
* ~~Better Exception Handling~~
* ~~Cache common queries/pages to ease load on Wikipedia and speed up response~~
* ~~Maybe: move all functionality to client-side, rewrite in angular~~ (decided against this move; If I'm going to rewrite it will be the backend in golang)
