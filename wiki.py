from flask import Flask, jsonify, request, render_template
import requests
from bs4 import BeautifulSoup
import re
import os
from urllib.parse import urlencode
from flask_cache import Cache

### CONFIG ###
app = Flask(__name__)
app.debug = True if 'True' in (os.environ.get('DEBUG', 'True')) else False

### DEBUG LOGGING ###
def DBLOG(printvar):
    if app.debug:
        print(printvar)
    return

### CACHING ###
if app.debug:
    DBLOG('Debug Mode: Using Simple Cache')
    cacheConfig = {'CACHE_TYPE': 'simple'}
else:
    cacheConfig = {
    'CACHE_TYPE': 'redis',
    'CACHE_KEY_PREFIX': 'wiki-philo',
    'CACHE_REDIS_URL': os.environ['REDIS_URL']
    }

cache = Cache(app,config=cacheConfig)

cacheTime = {
    'short': 60 * 6, # 3 minutes
    'medium': 60 * 60, # 1 hour
    'long': 60 * 60 * 24 # 24 hours
}

def Cache_Key():
    args = request.args
    key = request.path + '?' + urlencode([
        (k, v) for k in sorted(args) for v in sorted(args.getlist(k))
    ])
    return key


### HOME ROUTE ###
@app.route('/')
@cache.cached(timeout=cacheTime['long'], key_prefix='homepage')
def Wiki_Main():
    _context = {
        'data': {
            'debug': 'true' if app.debug else 'false'
        }
    }
    return render_template("wiki_main.html", **_context)

### API ROUTES ###
@app.route('/random/')
def Generate_Random():
    _baseUrl = "https://en.wikipedia.org/w/api.php?action=query&format=json&list=random&rnnamespace=0&rnlimit=1"
    results = Request_Random(_baseUrl)
    return jsonify(results)


@app.route('/wiki/', methods=['GET'])
@cache.cached(timeout=cacheTime['medium'], key_prefix=Cache_Key)
def Wiki_Search():
    search = request.args.get('s', default='New York City')
    end = False
    pathArr = []
    original = search
    retPage = Request_Page(search)
    soup = BeautifulSoup(retPage, 'html.parser')
    soup = Filter_HTML(soup)
    soup = BeautifulSoup(Strip_Brackets(soup), 'html.parser')
    nextPage = Parse_Links(soup)
    pathArr.append(nextPage)
    if nextPage['Title'] == "Philosophy":
        end = True
    else:
        search = nextPage['Title']

    return jsonify({'length': len(pathArr), 'original': original, 'end': end, 'nextPage': nextPage})
    

def Parse_Links(soup):
    nextPage = {} 
    nextPage['Url'] = False
    nextPage['Title'] = False
    nextPage['Link'] = False
    for link in soup.findAll('a'):
        linkFound = False
        nextPage['Link'] = str(link)
        # ignore wiktionary.org links
        if 'wiktionary' not in link['href']:
            for val, attr in link.attrs.items():
                if val == 'href':
                    nextPage['Url'] = attr
                if val == 'title':
                    nextPage['Title'] = attr
                    linkFound = True
            if linkFound:
                # remove '/wiki/ from Url
                DBLOG('\n' + nextPage['Url'] + '\n')
                nextPage['Url'] = nextPage['Url'].replace('/wiki/', '')
                nextPage['Url'] = nextPage['Url'].replace('/w/index.php?title=', '')
                nextPage['Url'] = nextPage['Url'].replace('&redirect=no', '')
                DBLOG('\n' + nextPage['Url'] + '\n')
                break
    return nextPage
    
### HELPER FUNCTIONS ###
def Request_Page(page):
    _baseUrl = "https://en.wikipedia.org/w/api.php"
    _queryString = {
        'action': 'parse',
        'section': 0,
        'prop': 'text',
        'format': 'json',
        'page': page
    }
    _fullUrl = _baseUrl + '?' + urlencode(_queryString)
    DBLOG(_fullUrl)
    r = requests.get(_fullUrl)
    DBLOG(r.status_code)
    res = r.json()
    retData = res['parse']['text']['*']

    return retData


def Request_Random(url):
    r = requests.get(url)
    res = r.json()
    return res['query']


def Filter_HTML(soup):
    extractArr = [
        'dl',
        'table',
        {'div': 'hatnote'},
        {'span': 'nowrap'},
        {'sup': 'reference'},
        {'div': 'thumb'},
        {'a': 'image'},
        {'span': 'IPA'},
        {'span': 'mw-redirectedfrom'},
        {'div': 'notice'},
        {'div': 'metadata'},
        {'sup': 'noprint'}
    ]

    for item in extractArr:
        if isinstance(item, dict):
            for extractKey in item.keys():
                [s.extract() for s in soup.findAll(extractKey, item[extractKey])]
        elif isinstance(item, str):
            [s.extract() for s in soup(item)]
    
    return soup


def Strip_Brackets(string):
    """
    remove brackets from a string
    leave brackets between "<a></a>" tags in place
    """
    string = "" + str(string)
    d = 0
    k = 0
    out = ''
    for i in string:
        #check for tag when not in parantheses mode
        if d < 1:
            if i == '>':
                k -= 1
            if i =="<":
                k +=  1
        #check for parentheses
        if k < 1:
            if i == '(':
                d += 1
            if d > 0:
                out += ' '
            else:
                out += i
            if i == ')' :
                d -= 1
        else:
            out +=i
    return out


if __name__ == '__main__':
    app.run(
        #host='0.0.0.0'
    )
